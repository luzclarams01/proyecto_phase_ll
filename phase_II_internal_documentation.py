#Members:
#Estefanía Pérez Hidalgo.
#Luz Clara Mora Salazar.
#Fabián Alfaro González.
import sys
import requests
import json
import pickle
import cognitive_face as CF
from PIL import Image, ImageDraw, ImageFont

subscription_key = None
SUBSCRIPTION_KEY = 'cdafd7788a4f4e178bc85046917a1b79'
BASE_URL = 'https://classes.cognitiveservices.azure.com/face/v1.0/'
CF.BaseUrl.set(BASE_URL)
CF.Key.set(SUBSCRIPTION_KEY)


def create_person(name, image_path, group_id):
    """This function is responsible for creating a person and add it to a 
    group depending on the characteristics of the person and the group."""
    response = CF.person.create(group_id, name)
    person_id = response['personId']
    CF.person.add_face(image_path, group_id, person_id)
    CF.person_group.train(group_id)
    response = CF.person_group.get_status(group_id)
    status = response['status']
    print(status)
    return person_id

def emotions(picture,bools):
    """It is in charge of analyzing the emotions through the microsoft azure system provided by the person's image."""
    image_path = picture
    image_data = open(image_path, "rb").read()
    headers = {'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
    'Content-Type': 'application/octet-stream'}
    params = {
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
    }
    response = requests.post(
                             BASE_URL + "detect/", headers=headers, params=params, data=image_data)
    analysis = response.json()
    dic = analysis[0]
    fa = dic['faceAttributes']
    if bools == True:
        gender = fa['gender']
        age = fa['age']
        return gender, age
    else:
        print(fa['emotion'])
"""Classess and inherintace."""
class Person():
    """It is in charge of providing the data to the people that 
    are going to be created, as personal data and extras."""
    def __init__(self, id_1, personId, name, gender, age, image_path):
        self.id_1 = id_1
        self. personId = personId
        self.name = name
        self.gender = gender
        self.age = age
        self.image_path = image_path
class Family(Person):
    """This function belongs to the family unit, since it 
    assigns a family with its respective data."""
    def __init__(self, id_1, personId, name, age, gender, image_path, pro_1, pro_2):
        super().__init__(id_1, personId, name, gender, age, image_path)
        self.relation = pro_1
        self.nationality = pro_2
class Friends(Person):
    """This function is of the dependence friends, 
    since it assigns a family with its respective data."""
    def __init__(self, id_1, personId, name, age, gender, image_path, pro_1, pro_2):
        super().__init__(id_1, personId, name, gender, age, image_path)
        self.start_year = pro_1
        self.nationality = pro_2
class Famous(Person):
    """This function is of the famous dependency, 
    since it assigns a family with its respective data."""
    def __init__(self, id_1, personId, name, age, gender, image_path, pro_1, pro_2):
        super().__init__(id_1, personId, name, gender, age, image_path)
        self.profession = pro_1
        self.description = pro_2

"""Function that allows to add the people to classes and groups"""
def create_person_in_group():
    """This function creates a person to and adds them to a 
    group, it can be added to any of the previous groups."""
    id_1 = input("\nEnter id: ", )
    name = input("Enter the person's name: ", )
    image_path = input("Enter the image path: ", )
    age, gender = emotions(image_path, True)
    print('\n\tEnter the name of the group where you want to add the person:')
    print('1 --> Family')
    print('2 --> Friends')
    print('3 --> Famous')
    group_id = int(input('Number typed: '))
    personId = create_person(name, image_path, group_id)
    if group_id == 1:
        pro_1 = input("\nEnter relationship between you and the person: ", )
        pro_2 = input("Enter the person's nationality: ", )
        person = Family(id_1, personId, name, gender, age, image_path, pro_1, pro_2)
        archivos_binarios(person)
        print("The person has been correctly entered.")
    elif group_id == 2:
        pro_1 = input("\nWhen did your friendship begin? Enter the date: ", )
        pro_2 = input("Enter the person's nationality: ", )
        person = Friends(id_1, personId, name, gender, age, image_path, pro_1, pro_2)
        archivos_binarios(person)
        print("The person has been correctly entered.")
    elif group_id ==3:
        pro_1 = input("\tEnter the person's profession: ", )
        pro_2 = input("Enter the description you perform: ", )
        person = Famous(id_1, personId, name, gender, age, image_path, pro_1, pro_2)
        archivos_binarios(person)
        print("The person has been correctly entered.")
      
def archivos_binarios(person):
    """Storage data in binary code, where people's data are located."""
    with open("person.bin", "ab") as f:
        pickle.dump(person, f, pickle.HIGHEST_PROTOCOL)
        print("It was successfully added.")

def read_file():
    """Read the binary data to request 
    registration of data on groups and individuals."""
    f = open("person.bin", "rb")
    f.seek(0)
    flag = 0
    list1=[]
    while flag ==0:
        try:
            e = pickle.load(f)
            list1.append([e.personId, e.name, e.age, e.gender])
        except:
            print("")
            flag = 1
    f.close
    return list1

def read_file1(person,group_id):
    """Read the binary data to request 
    registration of data on groups and individuals."""
    f = open("person.bin", "rb")
    f.seek(0)
    flag = 0
    while flag ==0:
        try:
            e = pickle.load(f)        
            if person==e.personId:
                print("The person's ID is: ", e.id_1, "\nThe personID is:", e.personId, '\nThe name is:', e.name, 
                '\nThe gender is:', e.gender, '\nThe age is:', e.age, '\nThe image path is:', e.image_path)
                if group_id=='1':
                    print('\nThe relation is:', e.relation, '\nThe nationality is:', e.nationality)
                elif group_id=='2':
                    print("\nThe realationship started in: ", e.start_year, '\nThe nationality:', e.nationality)
                elif group_id=='3':
                    print('\nThe profession is:', e.profession, '\nThe description is: ', e.description)
        except:
            print("")
            flag = 1
    f.close

def recognize_person(image_path, group_id):
    """Collect all the information from the 
    person's image pad in order to request the necessary data."""
    response = CF.face.detect(image_path)
    face_ids = [d['faceId'] for d in response]   
    identified_faces = CF.face.identify(face_ids, group_id)
    personas = identified_faces[0]
    candidates_list = personas['candidates']
    candidates = candidates_list[0]
    person = candidates['personId']
    person_data = CF.person.get(group_id, person)
    person_name = person_data['name']
    response = CF.face.detect(image_path)
    dic = response[0]
    faceRectangle = dic['faceRectangle']
    width = faceRectangle['width']
    top = faceRectangle['top']
    height = faceRectangle['height']
    left = faceRectangle['left']
    image=Image.open(image_path)
    draw = ImageDraw.Draw(image)
    draw.rectangle((left,top,left + width,top+height), outline='red')
    font = ImageFont.truetype('/Users/Usuario7/Desktop/imag/Arial_Unicode.ttf',50)
    draw.text((50, 50), person_name, font=font, fill="black")
    image.show()
    read_file1(person, group_id)

def print_people(group_id):  
    """Calls the person in the file list, 
    from the group where the person's id is located"""
    number_group=CF.person.lists(group_id)
    o=read_file()
    list1=[]
    for i in number_group:
        person_id=i['personId']
        for f in o:
            if f[0]==person_id:
                list1.append(f)
    return list1

def partition_list(l):
    """Order numbers from lowest to highest."""
    minors = list()
    greater = list()
    pivote = l[-1]
    for x in l[:-1]:
        if x < pivote:
            minors.append(x)
        else:
            greater.append(x)
    return([minors, pivote, greater])

def ordenamiento_quickSort(list0):
    """The sorting function is created by quickSort, which return a sorted list."""
    if len(list0)>0:
        list0 = partition_list(list0)
        cont = 0
        while cont < len(list0):
            e = list0[cont]
            if type(e) == list:
                if len(e) == 0:
                    del list0[cont]
                elif len(e) == 1:
                    list0[cont] = e[0]
                else:
                    list0 = list0[:cont]+partition_list(e) + list0[cont+1:]
            else:
                cont = cont + 1
    return (list0)

def ages(group_id):
    """Shows the ages of the people in the scanned image."""
    list1=print_people(group_id)
    ages = []
    """Browses through a list to find the age 
    in the attributes' list to add them to a new list."""
    for face in list1:
        age = face[2]
        if age >= 0:
            ages.append(age)
    return ages

def ages_order(group_id):
    """Funcion de ordenamineto por  edades """
    ages1=ages(group_id)
    ages2=ordenamiento_quickSort(ages1)
    return ages2

def sort_names_ages(group_id,case):
    """This function is in charge of gathering the information of the people 
    who are located in the groups to be able to order them respectively."""
    if case==1:
        age_upw2=[]
        n=1
        age_upw1=ages_order(group_id)
        list2=print_people(group_id)
        for ag in age_upw1:
            for people in list2:
                if ag==people[2]:
                    age_upw2.append(people)
                    print('Position', n, ':',people)
                    n+=1
                    list2.remove(people)
    if case==2:
        age_upw2=[]
        n=1
        age_upw1=ages_order(group_id)
        list2=print_people(group_id)
        for ag in age_upw1:
            for people in list2:
                if ag==people[2]:
                    age_upw2.append(people)
                    list2.remove(people)
        for k in age_upw2[::-1]:
            print('Position', n, ':',k)
            n+=1
    if case==3:
        names_upw2=[]
        n=1
        names_upw1=names_sorted(group_id)
        list2=print_people(group_id)
        for nm in names_upw1:
            for people in list2:
                if nm==people[1]:
                    names_upw2.append(people)
                    print('Position', n, ':',people)
                    n+=1
    if case==4:
        names_upw2=[]
        n=1
        names_upw1=names_sorted(group_id)
        list2=print_people(group_id)
        for nm in names_upw1:
            for people in list2:
                if nm==people[1]:
                    names_upw2.append(people)
        for k in names_upw2[::-1]:
            print('Position', n, ':',k)
            n+=1

def names_sorted(group_id):
    """It is in charge of ordering the names of the people, who are located in the groups."""
    list1=print_people(group_id)
    names = []
    """Browses through a list to find the names list to add them to a new list."""
    for n in list1:
        names.append(n[1])   
    list_names=[]
    a='A'
    cont=0
    for i in names:
        if i>a:
            list_names.append(i)
        elif i<list_names[0]:
            list_names.insert(0,i)
        else:
            for e in names:
                if i<e and i>names[cont+1]:
                    list_names.insert((cont+1),i)
                    break
                cont+=1
        a=list_names[-1]
    return(list_names)

while True:
    print("\n\tPick a request.")
    print("Type 1 -> Create a person in a group.")
    print("Type 2 -> Facial recognition.")
    print("Type 3 -> Print people information sorted by age or name.")
    case = int(input('Number typed: '))
    if case == 1:
        create_person_in_group()
    elif case == 2:
        image_path = input("\nType the image's path: ")
        print('\nGroups category:')
        print('1 --> Family')
        print('2 --> Friend')
        print('3 --> Famous')
        group_id = input("\nType the group ID: ")
        recognize_person(image_path, group_id)
    elif case == 3:
        print('\nGroups category:')
        print('1 --> Family')
        print('2 --> Friend')
        print('3 --> Famous')
        group_id = input("\nType the group ID: ")
        print("\n\tPick a request.")
        print("Type 1 -> Person's information sorted by ages upward.")
        print("Type 2 -> Person's information sorted by ages downward.")
        print("Type 3 -> Person's information sorted by names upward.")
        print("Type 4 -> Person's information sorted by names downward.")
        case = int(input('Number typed: '))
        sort_names_ages(group_id,case)